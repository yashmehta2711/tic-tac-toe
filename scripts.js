var currentPlayer = "x";
var status = "true";

const tdArray = document.getElementsByClassName("box");

for (let i = 0; i < tdArray.length; i++) {
    tdArray[i].addEventListener("click", function() {

        if (tdArray[i].innerHTML == "" && status == "true") {

            tdArray[i].innerHTML = currentPlayer;

            currentPlayer = currentPlayer == "x" ? "o" : "x";

            document.getElementById("playerNone").innerHTML = "Player " + currentPlayer.toUpperCase() + " your turn";

            if(
                tdArray[0].innerHTML == tdArray[1].innerHTML &&
                tdArray[1].innerHTML == tdArray[2].innerHTML &&
                tdArray[0].innerHTML != ""
            ){
                showWinner(0, 1, 2);
            }else if(
                tdArray[3].innerHTML == tdArray[4].innerHTML &&
                tdArray[4].innerHTML == tdArray[5].innerHTML &&
                tdArray[3].innerHTML != ""
            ){
                showWinner(3, 4, 5);
            }else if(
                tdArray[6].innerHTML == tdArray[7].innerHTML &&
                tdArray[7].innerHTML == tdArray[8].innerHTML &&
                tdArray[6].innerHTML != ""
            ){
                showWinner(6, 7, 8);
            }else if(
                tdArray[0].innerHTML == tdArray[3].innerHTML &&
                tdArray[3].innerHTML == tdArray[6].innerHTML &&
                tdArray[0].innerHTML != ""
            ){
                showWinner(0, 3, 6);
            }else if(
                tdArray[1].innerHTML == tdArray[4].innerHTML &&
                tdArray[4].innerHTML == tdArray[7].innerHTML &&
                tdArray[1].innerHTML != ""
            ){
                showWinner(1, 4, 7);
            }else if(
                tdArray[2].innerHTML == tdArray[5].innerHTML &&
                tdArray[5].innerHTML == tdArray[8].innerHTML &&
                tdArray[2].innerHTML != ""
            ){
                showWinner(2, 5, 8);
            }else if(
                tdArray[0].innerHTML == tdArray[4].innerHTML &&
                tdArray[4].innerHTML == tdArray[8].innerHTML &&
                tdArray[0].innerHTML != ""
            ){
                showWinner(0, 4, 8);
            }else if(
                tdArray[2].innerHTML == tdArray[4].innerHTML &&
                tdArray[4].innerHTML == tdArray[6].innerHTML &&
                tdArray[2].innerHTML != ""
            ){
                showWinner(2, 4, 6);
            }else if(tdArray[0].innerHTML != "" && tdArray[1].innerHTML != "" && tdArray[2].innerHTML != "" && tdArray[3].innerHTML != "" && tdArray[4].innerHTML != "" && tdArray[5].innerHTML != "" && tdArray[6].innerHTML != "" && tdArray[7].innerHTML != "" && tdArray[8].innerHTML != ""){
                document.getElementById("messageTie").style.display = "block";       
            }
        }
    });
}

document.getElementById("reset").addEventListener("click", function() {
    document.getElementById("playerNone").innerHTML = "Player X your turn";
    for(let i = 0; i < tdArray.length; i++){
        tdArray[i].innerHTML = "";
        tdArray[i].style.backgroundColor = "#FFF";
        tdArray[i].style.color = "#000";
        tdArray[i].onmouseover = function() {
            this.style.backgroundColor = "rgba(0, 0, 0, 0.2)";
        }
        tdArray[i].onmouseout = function() {
            this.style.backgroundColor = "#FFF";
        }
    }
    currentPlayer = "x";
    document.getElementById("message").style.display = "none";
    document.getElementById("messageTie").style.display = "none";
//    document.getElementById("player").innerHTML = "X";
    status = "true"; 
});

function showWinner(x, y, z){
    tdArray[x].style.background = "#333";
    tdArray[x].style.color = "#FFF";
    tdArray[y].style.background = "#333";
    tdArray[y].style.color = "#FFF";
    tdArray[z].style.background = "#333";
    tdArray[z].style.color = "#FFF";
    document.getElementById("winner").innerHTML = currentPlayer == "x" ? "O" : "X";
    document.getElementById("message").style.display = "block";
    document.getElementById("playerNone").innerHTML = "Game Over!";
    status = "false";
}